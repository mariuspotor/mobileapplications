/**
 * Created by mariuspotor on 14/11/2016.
 */
import Sessions from'../models/Sessions';
import Q from 'q';

const validateToken = function (res, token) {
  var deferred = Q.defer();
  
  Sessions.find({
    where: {
      token: token
    }
  }).then((session) => {
    if (!session.userid || session.expiration <= Date.now()) {
    deferred.reject("Unauthorized");
  } else {
    deferred.resolve(session.userid)
  }
}).catch(() => {
    deferred.reject("Unauthorized");
});
  return deferred.promise;
};

export {validateToken};
