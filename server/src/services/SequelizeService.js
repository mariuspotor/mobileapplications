/**
 * Created by mariuspotor on 14/11/2016.
 */
import Sequelize from 'sequelize';

const sequelize = new Sequelize('MobileApplications', 'root', '', {
  host   : 'localhost',
  dialect: 'mysql',
  port   : 3306
});

module.exports.getSequelize = function () {
  return sequelize;
};