/**
 * Created by mariuspotor on 14/11/2016.
 */
import Sequelize from 'sequelize';
import  sequelizeService from "../services/SequelizeService";

const sequelize = sequelizeService.getSequelize();

const Sessions = sequelize.define('Sessions', {
  id        : {
    type         : Sequelize.INTEGER,
    field        : 'id',
    primaryKey   : true,
    autoIncrement: true
  },
  userid    : {
    type : Sequelize.INTEGER,
    field: 'userid'
  },
  token     : {
    type : Sequelize.STRING,
    field: 'token'
  },
  expiration: {
    type : Sequelize.DATE,
    field: 'expiration'
  }
}, {
  freezeTableName: true
});

sequelize.sync();

export default Sessions;
