/**
 * Created by mariuspotor on 14/11/2016.
 */
import Sequelize from 'sequelize';
import  sequelizeService from "../services/SequelizeService";

const sequelize = sequelizeService.getSequelize();

const Phones = sequelize.define('Phones', {
  id     : {
    type : Sequelize.INTEGER,
    field: 'id',
    primaryKey   : true,
    autoIncrement: true
  },
  name       : {
    type : Sequelize.STRING,
    field: 'name'
  },
  description: {
    type : Sequelize.STRING,
    field: 'description'
  },
  price      : {
    type : Sequelize.INTEGER,
    field: 'price'
  }
}, {
  freezeTableName: true
});

sequelize.sync();

export default Phones;