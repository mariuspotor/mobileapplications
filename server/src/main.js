/**
 * Created by mariuspotor on 14/11/2016.
 */
import express from 'express';
import bodyParser from 'body-parser';
import listen from './main.listen'

const app = express();
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

import UsersEndpoint  from './endpoints/UsersEndpoint';
import SessionsEndpoint from './endpoints/SessionsEndpoint';
import PhonesEndpoint from './endpoints/PhonesEndpoint';

(new UsersEndpoint(app));
(new PhonesEndpoint(app));
(new SessionsEndpoint(app));

listen(app);