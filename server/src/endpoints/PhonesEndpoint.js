/**
 * Created by mariuspotor on 14/11/2016.
 */
import Phones from '../models/Phones';
// import Sessions from '../models/Sessions';
import {validateToken} from "../services/Authentication";


export default class PhonesEndpoint {
  constructor (app) {
    app.get('/phones/', function (req, res) {
      var response = {
        success : false
      };
      validateToken(res, req.headers['x-auth-token'])
        .then(() => {
          Phones.findAll().then((phones) => {
            let response = {
              success : true,
              data    : phones
            };
            res.status(200).send(response);
          }).catch(() => {
            res.status(401).send(response);
          });
        })
        .catch(() => {
          response.error = 'Unauthorized';
          res.status(401).send(response);
        });
    });
    
    app.get('/phones/:phoneId', function (req, res) {
      let response = {
        success : false
      };
      validateToken(res, req.headers['x-auth-token'])
        .then(() => {
          Phones.findById(req.params.phoneId).then((phone) => {
            let response = {
              success : true,
              data    : phone
            };
            res.status(200).send(response);
          }).catch(() => {
            let response = {
              success : false,
              data    : null
            };
            res.status(401).send(response);
          });
        }).catch(() => {
        response.error = 'Unauthorized';
        res.status(401).send(response);
      });
    });
    
    app.post('/phones/getByProperty', function (req, res) {
      let where    = {},
        property = req.body.property;
      
      where[property] = req.body.value;
      
      let response = {
        success : false
      };
      
      validateToken(res, req.headers['x-auth-token'])
        .then(() => {
          Phones.findAll({
            where : where
          }).then((phones) => {
            if (phones) {
              res.status(200).send({
                success : true,
                data    : phones
              });
            }
            else {
              res.status(200).send({
                success : false
              });
            }
          }).catch(() => {
            res.status(401).send({success : false});
          });
        }).catch(() => {
        response.error = 'Unauthorized';
        res.status(401).send(response);
      });
    });
    
    app.post('/phones', function (req, res) {
      let response = {
        success : false
      };
      validateToken(res, req.headers['x-auth-token'])
        .then(() => {
          Phones.create({
            name        : req.body.name,
            description : req.body.description,
            price       : req.body.price
          }).then((phone) => {
            let response = {
              success : true,
              data    : phone
            };
            res.status(200).send(response);
          }).catch(() => {
            let response = {
              success : false
            };
            res.status(401).send(response);
          });
        }).catch(() => {
        response.error = 'Unauthorized';
        res.status(401).send(response);
      });
    });
    
    app.put('/phones/:id', function (req, res) {
      let response = {
        success : false
      };
      validateToken(res, req.headers['x-auth-token'])
        .then(() => {
          Phones.findById(req.params.id).then((phone) => {
            phone.updateAttributes({
              name        : req.body.name,
              description : req.body.description,
              price       : req.body.price
            }).then(() => {
              let response = {
                success : true,
                data    : phone
              };
              res.status(200).send(response);
            }).catch(() => {
              let response = {
                success : false
              };
              res.status(200).send(response);
            });
          });
        }).catch(() => {
        response.error = 'Unauthorized';
        res.status(401).send(response);
      });
    });
    
    app.delete('/phones/:deletePhoneId', function (req, res) {
      let response = {
        success : false
      };
      validateToken(res, req.headers['x-auth-token'])
        .then(() => {
          Phones.findById(req.params.deletePhoneId).then((phone) => {
            let response = {
              success : true,
              data    : phone
            };
            phone.destroy();
            res.status(200).send(response);
          }).catch(() => {
            let response = {
              success : false
            };
            res.status(200).send(response);
          });
        }).catch(() => {
        response.error = 'Unauthorized';
        res.status(401).send(response);
      });
    });
  }
}
