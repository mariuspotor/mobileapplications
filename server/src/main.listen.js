/**
 * Created by mariuspotor on 14/11/2016.
 */
export default function (app) {
  app.listen(8081, function () {
    const host = this.address().address;
    const port = this.address().port;
    console.log("Example app listening at http://%s:%s", host, port)
  });
}